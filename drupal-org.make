; Drupal.org release file.
core = 7.24
api = 2

projects[ctools] = 1.3
projects[entity] = 1.2
projects[rules] = 2.3
projects[views] = 3.7
projects[addressfield] = 1.0-beta4
projects[commerce] = 1.8
